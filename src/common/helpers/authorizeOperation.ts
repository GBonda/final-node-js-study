import { ErrorMessages } from '../constants/ErrorMessages';
import { BalanceOperations } from '../enums/operations.enum';
import { Role } from '../enums/role.enum';
import { RequestWithUserRole } from '../interfaces/RequestWithUserRole';

export const authorizeOperation = (
  req: RequestWithUserRole,
  operation: BalanceOperations,
  guardedOperation: BalanceOperations,
  guardRole: Role,
) => {
  if (req && req?.user?.role !== guardRole && operation === guardedOperation) {
    return `${ErrorMessages.ACCESS_DENIED}: operation ${operation} is allowed only for ${guardRole}s`;
  }
  return null;
};
