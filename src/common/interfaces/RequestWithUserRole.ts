import { Request } from 'express';
import { User } from '../../entity/user.entity';

export interface RequestWithUserRole extends Request {
  user: User;
}
