export enum BalanceOperations {
  INCREASE = 'increase',
  DECREASE = 'decrease',
}
