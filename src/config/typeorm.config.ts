import { join } from 'path';

import { DataSource } from 'typeorm';

export default new DataSource({
  type: 'postgres',
  host: 'localhost',
  port: 5432,
  username: 'postgres',
  password: 'password',
  database: 'www-database',
  entities: ['./src/**/*.entity.ts'],
  migrations: [join(__dirname, '../migration/*.ts')],
  migrationsRun: true,
  synchronize: false,
});
