import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Profit } from '../../entity/profit.entity';
import { ProfitsController } from './profits.controller';
import { ProfitsService } from './profits.service';

@Module({
  imports: [TypeOrmModule.forFeature([Profit])],
  controllers: [ProfitsController],
  providers: [ProfitsService],
  exports: [ProfitsService],
})
export class ProfitsModule {}
