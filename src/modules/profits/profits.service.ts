import {
  ForbiddenException,
  HttpException,
  HttpStatus,
  Injectable,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ErrorMessages } from '../../common/constants/ErrorMessages';
import { LoggingMessages } from '../../common/constants/LoggingMessages';
import { BalanceOperations } from '../../common/enums/operations.enum';
import { Role } from '../../common/enums/role.enum';
import { authorizeOperation } from '../../common/helpers/authorizeOperation';
import { RequestWithUserRole } from '../../common/interfaces/RequestWithUserRole';
import { Profit } from '../../entity/profit.entity';
import { UpdateBalanceDto } from '../funds/dto/funds.dto';
import { CreateProfitDto, UpdateProfitDto } from './dto/profits.dto';

@Injectable()
export class ProfitsService {
  constructor(
    @InjectRepository(Profit)
    private readonly profitRepository: Repository<Profit>,
  ) {}

  private readonly logger = new Logger(ProfitsService.name);

  async startProfitProgram(createProfitDto: CreateProfitDto) {
    const maybeProfitProgram = await this.findOneByInvestorId(
      createProfitDto.investorId,
    );

    if (maybeProfitProgram) {
      this.logger.error(ErrorMessages.PROFIT_PROGRAM_EXISTS);
      throw new HttpException(
        ErrorMessages.PROFIT_PROGRAM_EXISTS,
        HttpStatus.BAD_REQUEST,
      );
    }

    const newProfitProgram = await this.profitRepository.save({
      investor: { id: createProfitDto.investorId },
      amount: 0,
    });

    return newProfitProgram;
  }

  async findAll() {
    return this.profitRepository.find({
      relations: {
        investor: true,
      },
    });
  }

  async findOneById(id: number): Promise<Profit> {
    const profit = await this.profitRepository.findOne({
      where: { id },
      relations: {
        investor: true,
      },
    });

    if (!profit) {
      this.logger.error(ErrorMessages.NOT_FOUND);
      throw new NotFoundException(ErrorMessages.NOT_FOUND);
    }

    return profit;
  }

  async findOneByInvestorId(investorId: number): Promise<Profit | null> {
    const profit = await this.profitRepository.findOne({
      where: { investor: { id: investorId } },
      relations: {
        investor: true,
      },
    });

    return profit;
  }

  async update(id: number, updateProfitDto: UpdateProfitDto) {
    const previousProfit = await this.findOneById(id);

    const updatedProfit = await this.profitRepository.save({
      ...previousProfit,
      ...updateProfitDto,
    });

    return updatedProfit;
  }

  async updateBalance(
    id: number,
    { amount, operation }: UpdateBalanceDto,
    req: RequestWithUserRole,
  ) {
    const decreaseAuthResult = authorizeOperation(
      req,
      operation,
      BalanceOperations.DECREASE,
      Role.User,
    );

    if (decreaseAuthResult) {
      this.logger.error(ErrorMessages.ACCESS_DENIED);
      throw new ForbiddenException(decreaseAuthResult);
    }

    const increaseAuthResult = authorizeOperation(
      req,
      operation,
      BalanceOperations.INCREASE,
      Role.Admin,
    );

    if (increaseAuthResult) {
      this.logger.error(ErrorMessages.ACCESS_DENIED);
      throw new ForbiddenException(increaseAuthResult);
    }

    const { amount: currentBalance } = await this.findOneById(id);

    if (operation === BalanceOperations.DECREASE && currentBalance < amount) {
      this.logger.error(
        `${ErrorMessages.NOT_ENOUGH_PROFIT}, current balance is ${currentBalance}`,
      );

      throw new HttpException(
        `${ErrorMessages.NOT_ENOUGH_PROFIT}, current balance is ${currentBalance}`,
        HttpStatus.BAD_REQUEST,
      );
    }

    const newBalance =
      operation === BalanceOperations.DECREASE
        ? currentBalance - amount
        : +currentBalance + amount;

    await this.profitRepository.update(id, { amount: newBalance });

    this.logger.log(
      `Profits account with id ${id} was successfully ${operation}d for: ${amount}`,
    );
  }

  async resetAll() {
    const allProfits = await this.findAll();

    for (const profit of allProfits) {
      await this.update(profit.id, { amount: 0 });
    }

    this.logger.log(LoggingMessages.PROFITS_RESET);
    return;
  }
}
