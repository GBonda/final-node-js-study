import { Test, TestingModule } from '@nestjs/testing';
import { ProfitsController } from './profits.controller';

describe('ProfitsController', () => {
  let controller: ProfitsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ProfitsController],
    }).compile();

    controller = module.get<ProfitsController>(ProfitsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
