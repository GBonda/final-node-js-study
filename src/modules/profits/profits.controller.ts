import {
  Body,
  Controller,
  Get,
  HttpCode,
  Param,
  ParseIntPipe,
  Patch,
  Req,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { ErrorMessages } from '../../common/constants/ErrorMessages';
import { Role } from '../../common/enums/role.enum';
import { RequestWithUserRole } from '../../common/interfaces/RequestWithUserRole';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { Roles } from '../auth/roles.decorator';
import { RolesGuard } from '../auth/roles.guard';
import { UpdateBalanceDto } from '../funds/dto/funds.dto';
import { ProfitResponseDto, UpdateProfitDto } from './dto/profits.dto';
import { ProfitsService } from './profits.service';

@ApiTags('profits')
@Controller('profits')
export class ProfitsController {
  constructor(private readonly profitsService: ProfitsService) {}

  @Roles(Role.Admin)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @HttpCode(200)
  @ApiOperation({ summary: 'get all profits' })
  @ApiBearerAuth()
  @Get()
  findAll() {
    return this.profitsService.findAll();
  }

  @Roles(Role.User, Role.Admin)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @HttpCode(200)
  @ApiOperation({ summary: 'get profit by id' })
  @ApiBearerAuth()
  @ApiNotFoundResponse({ description: ErrorMessages.NOT_FOUND })
  @ApiOkResponse({ type: ProfitResponseDto })
  @Get(':id')
  findOne(@Param('id', ParseIntPipe) id: number) {
    return this.profitsService.findOneById(id);
  }

  @Roles(Role.User, Role.Admin)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @HttpCode(200)
  @ApiOperation({ summary: 'get profit by investor id' })
  @ApiBearerAuth()
  @ApiNotFoundResponse({ description: ErrorMessages.NOT_FOUND })
  @ApiOkResponse({ type: ProfitResponseDto })
  @Get('/investor/:id')
  findOneByInvestor(@Param('id', ParseIntPipe) id: number) {
    return this.profitsService.findOneByInvestorId(id);
  }

  @Roles(Role.Admin)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @ApiOperation({ summary: 'update profit' })
  @ApiBearerAuth()
  @ApiBadRequestResponse({ description: ErrorMessages.BAD_REQUEST })
  @ApiNotFoundResponse({ description: ErrorMessages.NOT_FOUND })
  @Patch(':id')
  update(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateProfitDto: UpdateProfitDto,
  ) {
    return this.profitsService.update(id, updateProfitDto);
  }

  @Roles(Role.User, Role.Admin)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @ApiOperation({ summary: 'update profit balance' })
  @ApiBearerAuth()
  @ApiBadRequestResponse({ description: ErrorMessages.BAD_REQUEST })
  @ApiNotFoundResponse({ description: ErrorMessages.NOT_FOUND })
  @Patch('/balance/:id')
  updateBalance(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateBalanceDto: UpdateBalanceDto,
    @Req() req: RequestWithUserRole,
  ) {
    return this.profitsService.updateBalance(id, updateBalanceDto, req);
  }
}
