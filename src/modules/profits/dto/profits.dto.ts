import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsPositive } from 'class-validator';
import { ErrorMessages } from '../../../common/constants/ErrorMessages';
import { Investor } from '../../../entity/investor.entity';
import { InvestorResponseDto } from '../../investors/dto/investors.dto';

export class CreateProfitDto {
  @ApiProperty({ example: 1, description: 'investor id' })
  investorId: number;
}

export class UpdateProfitDto {
  @ApiProperty({ example: 100, description: 'funds amount' })
  @IsNumber({}, { message: ErrorMessages.NOT_NUMBER })
  @IsPositive()
  amount: number;
}

export class ProfitResponseDto {
  @ApiProperty({ example: 1, description: 'Fund id' })
  @IsNumber({}, { message: ErrorMessages.NOT_NUMBER })
  id: number;

  @ApiProperty({ example: 100, description: 'funds amount' })
  @IsNumber({}, { message: ErrorMessages.NOT_NUMBER })
  @IsPositive()
  amount: number;

  @ApiProperty({ type: InvestorResponseDto })
  investor: Investor;
}
