import { Test, TestingModule } from '@nestjs/testing';
import { InvitorsController } from './invitors.controller';

describe('InvitorsController', () => {
  let controller: InvitorsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [InvitorsController],
    }).compile();

    controller = module.get<InvitorsController>(InvitorsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
