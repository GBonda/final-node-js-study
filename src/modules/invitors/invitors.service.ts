import {
  HttpException,
  HttpStatus,
  Injectable,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ErrorMessages } from '../../common/constants/ErrorMessages';
import { Invitor } from '../../entity/invitor.entity';
import { InvestorsService } from '../investors/investors.service';
import { InviteesService } from '../invitees/invitees.service';
import { CreateInvitorDto } from './dto/invitors.dto';

@Injectable()
export class InvitorsService {
  constructor(
    @InjectRepository(Invitor)
    private readonly invitorRepository: Repository<Invitor>,

    private readonly investorsService: InvestorsService,

    private readonly inviteesService: InviteesService,
  ) {}

  private readonly logger = new Logger(InvitorsService.name);

  async createReferrerProfile(createInvitorDto: CreateInvitorDto) {
    const investor = await this.investorsService.findOneById(
      createInvitorDto.investorId,
    );

    if (!investor) {
      this.logger.error(ErrorMessages.NOT_FOUND);
      throw new HttpException(ErrorMessages.NOT_FOUND, HttpStatus.BAD_REQUEST);
    }

    if (investor.invitor !== null) {
      this.logger.error(ErrorMessages.INVITOR_ALREADY_EXISTS);
      throw new HttpException(
        ErrorMessages.INVITOR_ALREADY_EXISTS,
        HttpStatus.BAD_REQUEST,
      );
    }

    const newReferrer = await this.invitorRepository.save({
      investor: { id: createInvitorDto.investorId },
    });

    const newInvitee = await this.inviteesService.create({
      inviteeEmail: createInvitorDto.inviteeEmail,
      inviteeName: createInvitorDto.inviteeName,
      invitorId: newReferrer.id,
    });

    return { ...newReferrer, invitee: newInvitee.id };
  }

  async delete(id: number) {
    const invitorToDelete = await this.invitorRepository.findOne({
      where: { id },
    });

    if (!invitorToDelete) {
      this.logger.error(ErrorMessages.NOT_FOUND);
      throw new NotFoundException(ErrorMessages.NOT_FOUND);
    }

    await this.invitorRepository.remove(invitorToDelete);
    this.logger.log(`Invitor with id: ${id} successfully deleted`);
  }

  async findOneById(id: number): Promise<Invitor> {
    const invitor = await this.invitorRepository.findOne({
      where: { id },
      relations: ['investor', 'invitees'],
    });

    if (!invitor) {
      this.logger.error(ErrorMessages.NOT_FOUND);
      throw new NotFoundException(ErrorMessages.NOT_FOUND);
    }

    return invitor;
  }

  async findAll() {
    return this.invitorRepository.find({
      relations: ['investor', 'invitees'],
    });
  }

  async findInvitorsWithProfitableInvitees() {
    return this.invitorRepository
      .createQueryBuilder('invitor')
      .leftJoinAndSelect('invitor.invitees', 'invitees')
      .leftJoinAndSelect('invitor.investor', 'investorId')
      .leftJoinAndSelect('invitees.user', 'user')
      .leftJoinAndSelect('user.investor', 'investor')
      .where('investor IS NOT NULL')
      .getMany();
  }
}
