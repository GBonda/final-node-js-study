import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Fund } from '../../entity/fund.entity';
import { Investor } from '../../entity/investor.entity';
import { Invitee } from '../../entity/invitee.entity';
import { Invitor } from '../../entity/invitor.entity';
import { Profit } from '../../entity/profit.entity';
import { User } from '../../entity/user.entity';
import { FundsService } from '../funds/funds.service';
import { InvestorsService } from '../investors/investors.service';
import { InviteesService } from '../invitees/invitees.service';
import { ProfitsService } from '../profits/profits.service';
import { UsersService } from '../users/users.service';
import { InvitorsController } from './invitors.controller';
import { InvitorsService } from './invitors.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Invitor]),
    TypeOrmModule.forFeature([User]),
    TypeOrmModule.forFeature([Invitee]),
    TypeOrmModule.forFeature([Investor]),
    TypeOrmModule.forFeature([Profit]),
    TypeOrmModule.forFeature([Fund]),
  ],
  controllers: [InvitorsController],
  providers: [
    InvitorsService,
    InviteesService,
    UsersService,
    InvestorsService,
    FundsService,
    ProfitsService,
  ],
  exports: [InvitorsService, TypeOrmModule.forFeature([User])],
})
export class InvitorsModule {}
