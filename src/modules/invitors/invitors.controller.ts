import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  Param,
  ParseIntPipe,
  Post,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { ErrorMessages } from '../../common/constants/ErrorMessages';
import { Role } from '../../common/enums/role.enum';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { Roles } from '../auth/roles.decorator';
import { RolesGuard } from '../auth/roles.guard';
import { CreateInvitorDto, InvitorResponseDto } from './dto/invitors.dto';
import { InvitorsService } from './invitors.service';

@ApiTags('invitors')
@Controller('invitors')
export class InvitorsController {
  constructor(private readonly invitorsService: InvitorsService) {}

  @Roles(Role.User, Role.Admin)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @HttpCode(201)
  @ApiOperation({ summary: 'create invitors profile' })
  @ApiBearerAuth()
  @Post()
  create(@Body() createInvitorDto: CreateInvitorDto) {
    return this.invitorsService.createReferrerProfile(createInvitorDto);
  }

  @Roles(Role.Admin)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @HttpCode(200)
  @ApiOperation({ summary: 'get all invitors' })
  @ApiBearerAuth()
  @Get()
  findAll() {
    return this.invitorsService.findAll();
  }

  @Roles(Role.Admin)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @HttpCode(200)
  @ApiOperation({ summary: 'get invitors with profitable invitees' })
  @ApiBearerAuth()
  @Get('/with-profitable-invitees')
  findAllInvitorsWithProfitableInvitees() {
    return this.invitorsService.findInvitorsWithProfitableInvitees();
  }

  @Roles(Role.User, Role.Admin)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @HttpCode(200)
  @ApiOperation({ summary: 'get invitor by id' })
  @ApiBearerAuth()
  @ApiNotFoundResponse({ description: ErrorMessages.NOT_FOUND })
  @ApiOkResponse({ type: InvitorResponseDto })
  @Get(':id')
  findOne(@Param('id', ParseIntPipe) id: number) {
    return this.invitorsService.findOneById(id);
  }

  @Roles(Role.Admin)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Delete(':id')
  @ApiNotFoundResponse({ description: ErrorMessages.NOT_FOUND })
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Delete invitor by id' })
  @HttpCode(204)
  async remove(@Param('id', ParseIntPipe) id: number) {
    await this.invitorsService.delete(id);
  }
}
