import { Test, TestingModule } from '@nestjs/testing';
import { InvitorsService } from './invitors.service';

describe('InvitorsService', () => {
  let service: InvitorsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [InvitorsService],
    }).compile();

    service = module.get<InvitorsService>(InvitorsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
