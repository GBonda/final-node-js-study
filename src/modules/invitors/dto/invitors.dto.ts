import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsNumber } from 'class-validator';
import { ErrorMessages } from '../../../common/constants/ErrorMessages';
import { Investor } from '../../../entity/investor.entity';
import { Invitee } from '../../../entity/invitee.entity';
import { InvestorResponseDto } from '../../investors/dto/investors.dto';
import { InviteeResponseDto } from '../../invitees/dto/invitees.dto';

export class CreateInvitorDto {
  @ApiProperty({ example: 1, description: 'investor id' })
  @IsNumber({}, { message: ErrorMessages.NOT_NUMBER })
  investorId: number;

  @ApiProperty({ example: 'email@gmail.com', description: 'invitee email' })
  @IsEmail({}, { message: ErrorMessages.NOT_EMAIL })
  @IsNotEmpty({ message: ErrorMessages.EMPTY })
  inviteeEmail: string;

  @ApiProperty({ example: 'Sam', description: 'invitee name' })
  @IsNotEmpty({ message: ErrorMessages.EMPTY })
  inviteeName: string;
}

export class InvitorResponseDto {
  @ApiProperty({ example: 1, description: 'invitor id' })
  id: number;

  @ApiProperty({ type: InvestorResponseDto })
  investor: Investor;

  @ApiProperty({ type: Array(InviteeResponseDto) })
  invitees: Invitee[];
}
