import { Injectable, Logger, NotFoundException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from '../users/users.service';
import * as bcrypt from 'bcrypt';
import { ErrorMessages } from '../../common/constants/ErrorMessages';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  private readonly logger = new Logger(AuthService.name);

  async validateUser(username: string, pass: string): Promise<any> {
    const user = await this.usersService.findOne(username);
    if (!user) {
      if (!user) {
        this.logger.error(ErrorMessages.NOT_FOUND);
        throw new NotFoundException(ErrorMessages.NOT_FOUND);
      }
    }
    const { password, ...res } = user;
    const answer = await bcrypt.compare(pass, user.password);
    if (answer) return res;
    return null;
  }

  async login(user: any) {
    const payload = {
      username: user.username,
      sub: user.userId,
      role: user.role,
    };

    return {
      access_token: this.jwtService.sign(payload),
    };
  }
}
