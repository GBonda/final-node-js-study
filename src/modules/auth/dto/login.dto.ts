import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';
import { ErrorMessages } from '../../../common/constants/ErrorMessages';

export class LoginUserDto {
  @ApiProperty({ example: 'John', description: 'user name' })
  @IsNotEmpty({ message: ErrorMessages.EMPTY })
  username: string;

  @ApiProperty({ example: 'password', description: 'user password' })
  @IsString({ message: ErrorMessages.NOT_STRING })
  password?: string;
}
