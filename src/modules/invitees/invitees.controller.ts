import {
  Body,
  Controller,
  Get,
  HttpCode,
  Param,
  ParseIntPipe,
  Post,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { ErrorMessages } from '../../common/constants/ErrorMessages';
import { Role } from '../../common/enums/role.enum';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { Roles } from '../auth/roles.decorator';
import { RolesGuard } from '../auth/roles.guard';
import { CreateInviteeDto, InviteeResponseDto } from './dto/invitees.dto';
import { InviteesService } from './invitees.service';

@ApiTags('invitees')
@Controller('invitees')
export class InviteesController {
  constructor(private readonly inviteesService: InviteesService) {}

  @Roles(Role.User, Role.Admin)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @HttpCode(201)
  @ApiOperation({ summary: 'create new invitee' })
  @ApiBearerAuth()
  @Post()
  create(@Body() createInviteeDto: CreateInviteeDto) {
    return this.inviteesService.create(createInviteeDto);
  }

  @Roles(Role.Admin)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @HttpCode(200)
  @ApiOperation({ summary: 'get all invitees' })
  @ApiBearerAuth()
  @Get()
  findAll() {
    return this.inviteesService.findAll();
  }

  @Roles(Role.User, Role.Admin)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @HttpCode(200)
  @ApiOperation({ summary: 'get invitee by id' })
  @ApiBearerAuth()
  @ApiNotFoundResponse({ description: ErrorMessages.NOT_FOUND })
  @ApiOkResponse({ type: InviteeResponseDto })
  @Get(':id')
  findOne(@Param('id', ParseIntPipe) id: number) {
    return this.inviteesService.findOneById(id);
  }
}
