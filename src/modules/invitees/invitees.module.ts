import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Invitee } from '../../entity/invitee.entity';
import { User } from '../../entity/user.entity';
import { UsersService } from '../users/users.service';
import { InviteesController } from './invitees.controller';
import { InviteesService } from './invitees.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Invitee]),
    TypeOrmModule.forFeature([User]),
  ],
  controllers: [InviteesController],
  providers: [InviteesService, UsersService],
  exports: [InviteesService],
})
export class InviteesModule {}
