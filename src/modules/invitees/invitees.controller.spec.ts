import { Test, TestingModule } from '@nestjs/testing';
import { InviteesController } from './invitees.controller';

describe('InviteesController', () => {
  let controller: InviteesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [InviteesController],
    }).compile();

    controller = module.get<InviteesController>(InviteesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
