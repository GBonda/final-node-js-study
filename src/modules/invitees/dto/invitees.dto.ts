import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsNumber } from 'class-validator';
import { ErrorMessages } from '../../../common/constants/ErrorMessages';
import { Invitor } from '../../../entity/invitor.entity';
import { User } from '../../../entity/user.entity';
import { InvitorResponseDto } from '../../invitors/dto/invitors.dto';
import { UserResponseDto } from '../../users/dto/users.dto';

export class CreateInviteeDto {
  @ApiProperty({ example: 'email@gmail.com', description: 'invitee email' })
  @IsEmail({}, { message: ErrorMessages.NOT_EMAIL })
  @IsNotEmpty({ message: ErrorMessages.EMPTY })
  inviteeEmail: string;

  @ApiProperty({ example: 'Sam', description: 'invitee name' })
  @IsNotEmpty({ message: ErrorMessages.EMPTY })
  inviteeName: string;

  @ApiProperty({ example: 1, description: 'invitor id' })
  @IsNumber({}, { message: ErrorMessages.NOT_NUMBER })
  invitorId: number;
}

export class InviteeResponseDto {
  @ApiProperty({ example: 1, description: 'invitee id' })
  id: number;

  @ApiProperty({ type: UserResponseDto })
  user: User;

  @ApiProperty({ type: InvitorResponseDto })
  invitor: Invitor;
}
