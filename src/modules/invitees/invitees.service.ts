import {
  HttpException,
  HttpStatus,
  Injectable,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ErrorMessages } from '../../common/constants/ErrorMessages';
import { Invitee } from '../../entity/invitee.entity';
import { UsersService } from '../users/users.service';
import { CreateInviteeDto } from './dto/invitees.dto';

@Injectable()
export class InviteesService {
  constructor(
    @InjectRepository(Invitee)
    private readonly inviteeRepository: Repository<Invitee>,
    private readonly usersService: UsersService,
  ) {}

  private readonly logger = new Logger(InviteesService.name);

  async create(createInviteeDto: CreateInviteeDto) {
    const maybeUser = await this.usersService.getUserByEmail(
      createInviteeDto.inviteeEmail,
    );

    if (maybeUser) {
      this.logger.error(ErrorMessages.USER_IS_ALREADY_INVITED);
      throw new HttpException(
        ErrorMessages.USER_IS_ALREADY_INVITED,
        HttpStatus.BAD_REQUEST,
      );
    }

    const inviteesUserProfile = await this.usersService.create({
      username: createInviteeDto.inviteeName,
      email: createInviteeDto.inviteeEmail,
    });

    const newInvitee = await this.inviteeRepository.save({
      user: { id: inviteesUserProfile.id },
      invitor: { id: createInviteeDto.invitorId },
    });

    return newInvitee;
  }

  async findOneById(id: number): Promise<Invitee> {
    const invitee = await this.inviteeRepository.findOne({
      where: { id },
      relations: ['invitor', 'user'],
    });

    if (!invitee) {
      this.logger.error(ErrorMessages.NOT_FOUND);
      throw new NotFoundException(ErrorMessages.NOT_FOUND);
    }

    return invitee;
  }

  async findAll() {
    return this.inviteeRepository.find({
      relations: ['user', 'user.investor', 'invitor'],
    });
  }
}
