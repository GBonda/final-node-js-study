import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  Req,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { ErrorMessages } from '../../common/constants/ErrorMessages';
import { Role } from '../../common/enums/role.enum';
import { RequestWithUserRole } from '../../common/interfaces/RequestWithUserRole';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { Roles } from '../auth/roles.decorator';
import { RolesGuard } from '../auth/roles.guard';
import {
  CreateFundDto,
  DeleteManyFundsDto,
  FundResponseDto,
  UpdateBalanceDto,
  UpdateFundDto,
} from './dto/funds.dto';
import { FundsService } from './funds.service';

@ApiTags('funds')
@Controller('funds')
export class FundsController {
  constructor(private readonly fundsService: FundsService) {}

  @Roles(Role.Admin)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @HttpCode(201)
  @ApiOperation({ summary: 'open fund account' })
  @ApiBearerAuth()
  @Post()
  create(@Body() createFundDto: CreateFundDto) {
    return this.fundsService.openAccount(createFundDto);
  }

  @Roles(Role.Admin)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @HttpCode(200)
  @ApiOperation({ summary: 'get all funds' })
  @ApiBearerAuth()
  @Get()
  findAll() {
    return this.fundsService.findAll();
  }

  @Roles(Role.User, Role.Admin)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @HttpCode(200)
  @ApiOperation({ summary: 'get fund by id' })
  @ApiBearerAuth()
  @ApiNotFoundResponse({ description: ErrorMessages.NOT_FOUND })
  @ApiOkResponse({ type: FundResponseDto })
  @Get(':id')
  findOne(@Param('id', ParseIntPipe) id: number) {
    return this.fundsService.findOneById(id);
  }
  @Roles(Role.Admin)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @ApiOperation({ summary: 'reset funds' })
  @ApiBearerAuth()
  @ApiBadRequestResponse({ description: ErrorMessages.BAD_REQUEST })
  @Patch('/reset')
  reset() {
    return this.fundsService.resetAll();
  }

  @Roles(Role.Admin)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @ApiOperation({ summary: 'update fund' })
  @ApiBearerAuth()
  @ApiBadRequestResponse({ description: ErrorMessages.BAD_REQUEST })
  @ApiNotFoundResponse({ description: ErrorMessages.NOT_FOUND })
  @Patch(':id')
  update(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateFundDto: UpdateFundDto,
  ) {
    return this.fundsService.update(id, updateFundDto);
  }

  @Roles(Role.User, Role.Admin)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @ApiOperation({ summary: 'update fund balance' })
  @ApiBearerAuth()
  @ApiBadRequestResponse({ description: ErrorMessages.BAD_REQUEST })
  @ApiNotFoundResponse({ description: ErrorMessages.NOT_FOUND })
  @Patch('/balance/:id')
  updateBalance(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateBalanceDto: UpdateBalanceDto,
    @Req() req: RequestWithUserRole,
  ) {
    return this.fundsService.updateBalance(id, updateBalanceDto, req);
  }

  @Roles(Role.Admin)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Delete(':id')
  @ApiNotFoundResponse({ description: ErrorMessages.NOT_FOUND })
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Delete fund by id' })
  @HttpCode(204)
  async remove(@Param('id', ParseIntPipe) id: number) {
    await this.fundsService.delete(id);
  }

  @Roles(Role.Admin)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Delete()
  @ApiNotFoundResponse({ description: ErrorMessages.NOT_FOUND })
  @ApiOperation({ summary: 'Delete funds by ids' })
  @ApiBearerAuth()
  @HttpCode(204)
  async removeMany(@Body() deleteIdsDto: DeleteManyFundsDto) {
    return this.fundsService.deleteMany(deleteIdsDto);
  }
}
