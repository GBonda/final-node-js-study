import {
  HttpStatus,
  Injectable,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { ForbiddenException, HttpException } from '@nestjs/common/exceptions';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Repository } from 'typeorm';
import { ErrorMessages } from '../../common/constants/ErrorMessages';
import { LoggingMessages } from '../../common/constants/LoggingMessages';
import { BalanceOperations } from '../../common/enums/operations.enum';
import { Role } from '../../common/enums/role.enum';
import { authorizeOperation } from '../../common/helpers/authorizeOperation';
import { RequestWithUserRole } from '../../common/interfaces/RequestWithUserRole';
import { Fund } from '../../entity/fund.entity';
import { ProfitsService } from '../profits/profits.service';
import {
  CreateFundDto,
  DeleteManyFundsDto,
  UpdateBalanceDto,
  UpdateFundDto,
} from './dto/funds.dto';

@Injectable()
export class FundsService {
  constructor(
    @InjectRepository(Fund)
    private readonly fundRepository: Repository<Fund>,

    private readonly profitsService: ProfitsService,
  ) {}

  private readonly logger = new Logger(FundsService.name);

  async openAccount(createFundDto: CreateFundDto): Promise<Fund> {
    const newFund = await this.fundRepository.save({
      investor: { id: createFundDto.investorId },
      amount: createFundDto.amount,
    });

    return newFund;
  }

  async findAll() {
    return this.fundRepository.find({
      relations: {
        investor: true,
      },
    });
  }

  async findOneById(id: number): Promise<Fund> {
    const fund = await this.fundRepository.findOne({
      where: { id },
      relations: {
        investor: true,
      },
    });

    if (!fund) {
      this.logger.error(ErrorMessages.NOT_FOUND);
      throw new NotFoundException(ErrorMessages.NOT_FOUND);
    }

    return fund;
  }

  async findManyByIds(ids: number[]): Promise<Fund[]> {
    const foundFunds = await this.fundRepository.find({
      where: { id: In(ids) },
    });

    if (!foundFunds?.length) {
      this.logger.error(`${ErrorMessages.NOT_FOUND}`);
      throw new NotFoundException(`${ErrorMessages.NOT_FOUND}`);
    }

    return foundFunds;
  }

  async update(id: number, updateFundDto: UpdateFundDto) {
    const previousFund = await this.findOneById(id);

    const updatedFund = await this.fundRepository.save({
      ...previousFund,
      ...updateFundDto,
    });

    return updatedFund;
  }

  async updateBalance(
    id: number,
    { amount, operation }: UpdateBalanceDto,
    req: RequestWithUserRole,
  ) {
    const decreaseAuthResult = authorizeOperation(
      req,
      operation,
      BalanceOperations.DECREASE,
      Role.Admin,
    );

    if (decreaseAuthResult) {
      this.logger.error(ErrorMessages.ACCESS_DENIED);
      throw new ForbiddenException(decreaseAuthResult);
    }

    const increaseAuthResult = authorizeOperation(
      req,
      operation,
      BalanceOperations.INCREASE,
      Role.User,
    );

    if (increaseAuthResult) {
      this.logger.error(ErrorMessages.ACCESS_DENIED);
      throw new ForbiddenException(increaseAuthResult);
    }

    const { amount: currentBalance } = await this.findOneById(id);

    if (operation === BalanceOperations.DECREASE && currentBalance < amount) {
      this.logger.error(
        `${ErrorMessages.NOT_ENOUGH_FUNDS}, current balance is ${currentBalance}`,
      );

      throw new HttpException(
        `${ErrorMessages.NOT_ENOUGH_FUNDS}, current balance is ${currentBalance}`,
        HttpStatus.BAD_REQUEST,
      );
    }

    const newBalance =
      operation === BalanceOperations.DECREASE
        ? currentBalance - amount
        : +currentBalance + amount;

    await this.fundRepository.update(id, { amount: newBalance });

    this.logger.log(
      `Funds account with id ${id} was successfully ${operation}d for: ${amount}`,
    );
  }

  async delete(id: number) {
    const investorToDelete = await this.fundRepository.findOne({
      where: { id },
    });

    if (!investorToDelete) {
      this.logger.error(ErrorMessages.NOT_FOUND);
      throw new NotFoundException(ErrorMessages.NOT_FOUND);
    }

    await this.fundRepository.remove(investorToDelete);
    this.logger.log(`Fund with id: ${id} successfully deleted`);
  }

  async deleteMany({ ids }: DeleteManyFundsDto) {
    const sizesToDelete = await this.findManyByIds(ids);

    await this.fundRepository.remove(sizesToDelete);
    this.logger.log(`Funds ${ids.join(', ')} successfully deleted`);
  }

  async resetAll() {
    const allFunds = await this.findAll();

    for (const fund of allFunds) {
      await this.update(fund.id, { amount: 0 });
    }

    this.logger.log(LoggingMessages.FUNDS_RESET);

    await this.profitsService.resetAll();

    return;
  }
}
