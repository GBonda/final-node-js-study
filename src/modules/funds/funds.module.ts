import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Fund } from '../../entity/fund.entity';
import { Profit } from '../../entity/profit.entity';
import { ProfitsService } from '../profits/profits.service';
import { FundsController } from './funds.controller';
import { FundsService } from './funds.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Fund]),
    TypeOrmModule.forFeature([Profit]),
  ],
  controllers: [FundsController],
  providers: [FundsService, ProfitsService],
  exports: [FundsService],
})
export class FundsModule {}
