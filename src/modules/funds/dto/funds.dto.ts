import { ApiProperty } from '@nestjs/swagger';
import {
  ArrayNotEmpty,
  IsEnum,
  IsNotEmpty,
  IsNumber,
  IsPositive,
} from 'class-validator';
import { ErrorMessages } from '../../../common/constants/ErrorMessages';
import { BalanceOperations } from '../../../common/enums/operations.enum';
import { Investor } from '../../../entity/investor.entity';
import { InvestorResponseDto } from '../../investors/dto/investors.dto';

export class CreateFundDto {
  @ApiProperty({ example: 1, description: 'investor id' })
  @IsNumber({}, { message: ErrorMessages.NOT_NUMBER })
  investorId: number;

  @ApiProperty({ example: 100, description: 'funds amount' })
  @IsNumber({}, { message: ErrorMessages.NOT_NUMBER })
  @IsPositive()
  amount: number;
}

export class UpdateFundDto {
  @ApiProperty({ example: 100, description: 'funds amount' })
  @IsNumber({}, { message: ErrorMessages.NOT_NUMBER })
  @IsPositive()
  amount: number;
}

export class FundResponseDto extends CreateFundDto {
  @ApiProperty({ type: InvestorResponseDto })
  investor: Investor;
}

export class DeleteManyFundsDto {
  @ApiProperty({ example: [1, 2], description: 'Array ids for delete' })
  @ArrayNotEmpty()
  @IsNumber({}, { message: ErrorMessages.NOT_NUMBER, each: true })
  ids: number[];
}

export class UpdateBalanceDto {
  @ApiProperty({ example: 100, description: 'amount of money' })
  @IsNotEmpty({ message: ErrorMessages.EMPTY })
  @IsPositive({ message: ErrorMessages.NOT_POSITIVE_NUMBER })
  amount: number;

  @ApiProperty({ example: 'increase', description: 'operation' })
  @IsEnum(BalanceOperations, {
    message: `allowed operations are: ${BalanceOperations.INCREASE} and ${BalanceOperations.DECREASE}`,
  })
  operation: BalanceOperations;
}
