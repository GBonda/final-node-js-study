import { ApiProperty } from '@nestjs/swagger';
import {
  IsDateString,
  IsNotEmpty,
  IsNumber,
  IsPositive,
} from 'class-validator';
import { ErrorMessages } from '../../../common/constants/ErrorMessages';

export class CreateInvestorDto {
  @ApiProperty({ example: 1, description: 'user id' })
  @IsNumber({}, { message: ErrorMessages.NOT_NUMBER })
  userId: number;

  @ApiProperty({ example: 100, description: 'investition amount' })
  @IsNumber({}, { message: ErrorMessages.NOT_NUMBER })
  @IsPositive()
  investition: number;
}

export class InvestorResponseDto {
  @ApiProperty({ example: 1, description: 'investor id' })
  id: number;

  @ApiProperty({
    example: '2022-12-29T10:42:34.243Z',
    description: 'creation date',
  })
  @IsDateString({ message: ErrorMessages.NOT_DATE })
  @IsNotEmpty({ message: ErrorMessages.EMPTY })
  createdAt: string;

  @ApiProperty({
    example: '2022-12-29T10:42:34.243Z',
    description: 'updates date',
  })
  @IsDateString({ message: ErrorMessages.NOT_DATE })
  @IsNotEmpty({ message: ErrorMessages.EMPTY })
  updatedAt: string;
}
