import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Fund } from '../../entity/fund.entity';
import { Investor } from '../../entity/investor.entity';
import { Profit } from '../../entity/profit.entity';
import { FundsService } from '../funds/funds.service';
import { ProfitsService } from '../profits/profits.service';
import { InvestorsController } from './investors.controller';
import { InvestorsService } from './investors.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Investor]),
    TypeOrmModule.forFeature([Fund]),
    TypeOrmModule.forFeature([Profit]),
  ],
  controllers: [InvestorsController],
  providers: [InvestorsService, FundsService, ProfitsService],
  exports: [InvestorsService],
})
export class InvestorsModule {}
