import {
  HttpException,
  HttpStatus,
  Injectable,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ErrorMessages } from '../../common/constants/ErrorMessages';
import { Investor } from '../../entity/investor.entity';
import { FundsService } from '../funds/funds.service';
import { ProfitsService } from '../profits/profits.service';
import { CreateInvestorDto } from './dto/investors.dto';

@Injectable()
export class InvestorsService {
  constructor(
    @InjectRepository(Investor)
    private readonly investorRepository: Repository<Investor>,

    private readonly fundsService: FundsService,

    private readonly profitsService: ProfitsService,
  ) {}

  private readonly logger = new Logger(InvestorsService.name);

  async createInvestorsProfile(createInvestorDto: CreateInvestorDto) {
    const maybeInvestor = await this.findOneByUserId(createInvestorDto.userId);

    if (maybeInvestor) {
      this.logger.error(ErrorMessages.INVESTOR_ALREADY_EXISTS);
      throw new HttpException(
        ErrorMessages.INVESTOR_ALREADY_EXISTS,
        HttpStatus.BAD_REQUEST,
      );
    }

    const newInvestor = await this.investorRepository.save({
      user: { id: createInvestorDto.userId },
    });

    const newFundsAccount = await this.fundsService.openAccount({
      investorId: newInvestor.id,
      amount: createInvestorDto.investition,
    });

    const newProfitProgram = await this.profitsService.startProfitProgram({
      investorId: newInvestor.id,
    });

    return {
      ...newInvestor,
      funds: newFundsAccount.amount,
      profit: newProfitProgram.amount,
    };
  }

  async findAll() {
    return this.investorRepository.find({ relations: ['user'] });
  }

  async findOneById(id: number): Promise<Investor> {
    const investor = await this.investorRepository.findOne({
      where: { id },
      relations: ['profit', 'fund', 'invitor'],
    });

    if (!investor) {
      this.logger.error(ErrorMessages.NOT_FOUND);
      throw new NotFoundException(ErrorMessages.NOT_FOUND);
    }

    return investor;
  }

  async findOneByUserId(userId: number): Promise<Investor | null> {
    const investor = await this.investorRepository.findOne({
      where: { user: { id: userId } },
    });

    return investor;
  }

  async delete(id: number) {
    const investorToDelete = await this.investorRepository.findOne({
      where: { id },
    });

    if (!investorToDelete) {
      this.logger.error(ErrorMessages.NOT_FOUND);
      throw new NotFoundException(ErrorMessages.NOT_FOUND);
    }

    await this.investorRepository.remove(investorToDelete);
    this.logger.log(`Investor with id: ${id} successfully deleted`);
  }
}
