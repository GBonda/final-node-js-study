import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  Param,
  ParseIntPipe,
  Post,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { ErrorMessages } from '../../common/constants/ErrorMessages';
import { Role } from '../../common/enums/role.enum';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { Roles } from '../auth/roles.decorator';
import { RolesGuard } from '../auth/roles.guard';
import { CreateInvestorDto, InvestorResponseDto } from './dto/investors.dto';
import { InvestorsService } from './investors.service';

@ApiTags('investors')
@Controller('investors')
export class InvestorsController {
  constructor(private readonly investorsService: InvestorsService) {}

  @Roles(Role.User)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @HttpCode(201)
  @ApiOperation({ summary: 'create investors profile' })
  @ApiBearerAuth()
  @Post()
  create(@Body() createInvestorDto: CreateInvestorDto) {
    return this.investorsService.createInvestorsProfile(createInvestorDto);
  }

  @Roles(Role.User, Role.Admin)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @HttpCode(200)
  @ApiOperation({ summary: 'get investor by id' })
  @ApiBearerAuth()
  @ApiNotFoundResponse({ description: ErrorMessages.NOT_FOUND })
  @ApiOkResponse({ type: InvestorResponseDto })
  @Get(':id')
  findOne(@Param('id', ParseIntPipe) id: number) {
    return this.investorsService.findOneById(id);
  }

  @Roles(Role.Admin)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @HttpCode(200)
  @ApiOperation({ summary: 'get all investors' })
  @ApiBearerAuth()
  @Get()
  findAll() {
    return this.investorsService.findAll();
  }

  @Roles(Role.Admin)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Delete(':id')
  @ApiNotFoundResponse({ description: ErrorMessages.NOT_FOUND })
  @ApiBearerAuth()
  @ApiOperation({ summary: 'Delete investor by id' })
  @HttpCode(204)
  async remove(@Param('id', ParseIntPipe) id: number) {
    await this.investorsService.delete(id);
  }
}
