import { ApiProperty, PartialType } from '@nestjs/swagger';
import {
  IsDateString,
  IsEmail,
  IsNotEmpty,
  IsNumber,
  IsString,
} from 'class-validator';
import { ErrorMessages } from '../../../common/constants/ErrorMessages';
import { Role } from '../../../common/enums/role.enum';
import { Investor } from '../../../entity/investor.entity';
import { InvestorResponseDto } from '../../investors/dto/investors.dto';

export class CreateUserDto {
  @ApiProperty({ example: 'John', description: 'user name' })
  @IsNotEmpty({ message: ErrorMessages.EMPTY })
  username: string;

  @ApiProperty({ example: 'password', description: 'user password' })
  @IsString({ message: ErrorMessages.NOT_STRING })
  password?: string;

  @ApiProperty({ example: 'john@gmail.com', description: 'user email' })
  @IsString({ message: ErrorMessages.NOT_STRING })
  @IsNotEmpty({ message: ErrorMessages.EMPTY })
  @IsEmail({ message: ErrorMessages.INVALID_EMAIL })
  email: string;

  @ApiProperty({ example: 'user', description: 'user role' })
  @IsString({ message: ErrorMessages.NOT_STRING })
  role?: Role;
}

export class UpdateUserDto extends PartialType(CreateUserDto) {}

export class UserResponseDto extends CreateUserDto {
  @ApiProperty({ example: 1, description: 'User id' })
  @IsNumber()
  id: number;

  @ApiProperty({
    example: '2022-12-29T10:42:34.243Z',
    description: 'creation date',
  })
  @IsDateString({ message: ErrorMessages.NOT_DATE })
  @IsNotEmpty({ message: ErrorMessages.EMPTY })
  createdAt: string;

  @ApiProperty({
    example: '2022-12-29T10:42:34.243Z',
    description: 'updates date',
  })
  @IsDateString({ message: ErrorMessages.NOT_DATE })
  @IsNotEmpty({ message: ErrorMessages.EMPTY })
  updatedAt: string;

  @ApiProperty({ type: InvestorResponseDto })
  investor: Investor;
}
