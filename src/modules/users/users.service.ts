import {
  ForbiddenException,
  HttpException,
  HttpStatus,
  Injectable,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ErrorMessages } from '../../common/constants/ErrorMessages';
import { User } from '../../entity/user.entity';
import { CreateUserDto, UpdateUserDto } from './dto/users.dto';
import * as bcrypt from 'bcrypt';
import { SALT_ROUNDS } from '../../common/constants/SaltRounds';
import { RequestWithUserRole } from '../../common/interfaces/RequestWithUserRole';
import { Role } from '../../common/enums/role.enum';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) {}

  private readonly logger = new Logger(UsersService.name);

  async getUserByEmail(email: string) {
    return await this.userRepository.findOne({
      where: { email },
    });
  }

  async create(createUserDto: CreateUserDto, req?: RequestWithUserRole) {
    if (createUserDto.role === Role.Admin && req?.user?.role !== Role.Admin) {
      this.logger.error(ErrorMessages.ACCESS_DENIED);
      throw new ForbiddenException(ErrorMessages.ACCESS_DENIED);
    }

    const maybeUser = await this.getUserByEmail(createUserDto.email);

    if (maybeUser) {
      this.logger.error(ErrorMessages.USER_ALREADY_EXISTS);
      throw new HttpException(
        ErrorMessages.USER_ALREADY_EXISTS,
        HttpStatus.BAD_REQUEST,
      );
    }

    const userData = createUserDto;

    if (createUserDto.password) {
      const hashedPassword = await bcrypt.hash(
        createUserDto.password,
        SALT_ROUNDS,
      );
      userData.password = hashedPassword;
    }

    const newUser = await this.userRepository.save(userData);

    this.logger.log(`New user profile for ${newUser.username} created`);

    return newUser;
  }

  async findAll() {
    return this.userRepository.find();
  }

  async findOne(username: string): Promise<User | null> {
    const user = await this.userRepository.findOne({
      where: { username },
    });

    return user;
  }

  async findOneById(id: number): Promise<User> {
    const user = await this.userRepository.findOne({
      where: { id },
      relations: ['investor'],
    });

    if (!user) {
      this.logger.error(ErrorMessages.NOT_FOUND);
      throw new NotFoundException(ErrorMessages.NOT_FOUND);
    }

    return user;
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    const oldUser = await this.findOneById(id);

    const updatedUserData = { ...oldUser, ...updateUserDto };

    if (updateUserDto.password) {
      const hashedPassword = await bcrypt.hash(
        updateUserDto.password,
        SALT_ROUNDS,
      );
      updatedUserData.password = hashedPassword;
    }

    updatedUserData.role = oldUser.role;

    const updatedUser = await this.userRepository.save(updatedUserData);

    this.logger.log(`User profile for ${updatedUser.username} updated`);

    return updatedUser;
  }
}
