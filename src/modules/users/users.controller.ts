import {
  Body,
  Controller,
  Get,
  HttpCode,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  Req,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { ErrorMessages } from '../../common/constants/ErrorMessages';
import { Role } from '../../common/enums/role.enum';
import { RequestWithUserRole } from '../../common/interfaces/RequestWithUserRole';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { Roles } from '../auth/roles.decorator';
import { RolesGuard } from '../auth/roles.guard';
import { CreateUserDto, UpdateUserDto, UserResponseDto } from './dto/users.dto';
import { UsersService } from './users.service';

@ApiTags('users')
@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @HttpCode(201)
  @ApiOperation({ summary: 'create new user' })
  @ApiBearerAuth()
  @Post()
  create(
    @Body() createUserDto: CreateUserDto,
    @Req() req: RequestWithUserRole,
  ) {
    return this.usersService.create(createUserDto, req);
  }

  @Roles(Role.User, Role.Admin)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @HttpCode(200)
  @ApiBearerAuth()
  @ApiOperation({ summary: 'get all users' })
  @Get()
  findAll() {
    return this.usersService.findAll();
  }

  @HttpCode(200)
  @ApiOperation({ summary: 'get user by id' })
  @ApiNotFoundResponse({ description: ErrorMessages.NOT_FOUND })
  @ApiOkResponse({ type: UserResponseDto })
  @Get(':id')
  findOne(@Param('id', ParseIntPipe) id: number) {
    return this.usersService.findOneById(id);
  }

  @Roles(Role.User, Role.Admin)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @ApiOperation({ summary: 'update user' })
  @ApiBearerAuth()
  @ApiBadRequestResponse({ description: ErrorMessages.BAD_REQUEST })
  @ApiNotFoundResponse({ description: ErrorMessages.NOT_FOUND })
  @Patch(':id')
  update(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateUserDto: UpdateUserDto,
  ) {
    return this.usersService.update(id, updateUserDto);
  }
}
