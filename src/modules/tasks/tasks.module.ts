import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Fund } from '../../entity/fund.entity';
import { Investor } from '../../entity/investor.entity';
import { Invitee } from '../../entity/invitee.entity';
import { Invitor } from '../../entity/invitor.entity';
import { Profit } from '../../entity/profit.entity';
import { User } from '../../entity/user.entity';
import { FundsService } from '../funds/funds.service';
import { InvestorsService } from '../investors/investors.service';
import { InviteesService } from '../invitees/invitees.service';
import { InvitorsModule } from '../invitors/invitors.module';
import { InvitorsService } from '../invitors/invitors.service';
import { ProfitsService } from '../profits/profits.service';
import { UsersModule } from '../users/users.module';
import { UsersService } from '../users/users.service';
import { TasksService } from './tasks.service';

@Module({
  providers: [
    TasksService,
    FundsService,
    ProfitsService,
    InvitorsService,
    InvestorsService,
    UsersService,
    InviteesService,
  ],
  imports: [
    TypeOrmModule.forFeature([Invitor]),
    TypeOrmModule.forFeature([Fund]),
    TypeOrmModule.forFeature([Profit]),
    TypeOrmModule.forFeature([User]),
    TypeOrmModule.forFeature([Investor]),
    TypeOrmModule.forFeature([Invitee]),
    UsersModule,
    InvitorsModule,
  ],
})
export class TasksModule {}
