import { Injectable, Logger } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import {
  DAILY_PROFIT_PERCENT,
  REFERRAL_PERCENT,
} from '../../common/constants/profitsPercents';
import { Fund } from '../../entity/fund.entity';
import { Invitee } from '../../entity/invitee.entity';
import { FundsService } from '../funds/funds.service';
import { InvitorsService } from '../invitors/invitors.service';
import { ProfitsService } from '../profits/profits.service';

@Injectable()
export class TasksService {
  constructor(
    private readonly invitorsService: InvitorsService,
    private readonly fundsService: FundsService,
    private readonly profitsService: ProfitsService,
  ) {}

  private readonly logger = new Logger(TasksService.name);

  @Cron('0 0 1 * * *')
  async handleCron() {
    this.logger.debug(
      `Cron job for accrual of profits activated, ${new Date().toLocaleString()}`,
    );

    const allFunds = await this.fundsService.findAll();
    for (const fund of allFunds) {
      const increaseAmount = (fund.amount * DAILY_PROFIT_PERCENT) / 100;
      await this.increaseFundFor(increaseAmount, fund);
    }

    this.logger.log(
      `Investors' profits are accrued: +${DAILY_PROFIT_PERCENT}% of own funds`,
    );

    const invitorsWithProfitableInvitees =
      await this.invitorsService.findInvitorsWithProfitableInvitees();

    for (const invitor of invitorsWithProfitableInvitees) {
      const fund = await this.fundsService.findOneById(invitor.investor.id);
      const increaseAmount = await this.calculateReferrerProfit(
        invitor.invitees,
      );
      await this.increaseFundFor(increaseAmount, fund);
    }

    this.logger.log(
      `Referrers' profits are accrued: +${REFERRAL_PERCENT}% of referrees' profits`,
    );
  }

  async calculateReferrerProfit(invitees: Invitee[]) {
    return invitees.reduce(async (prevVal, currentVal) => {
      const inviteeProfit = await this.profitsService.findOneById(
        currentVal.user.investor.id,
      );
      const previousValue = await prevVal;
      return (
        previousValue + (Number(inviteeProfit.amount) * REFERRAL_PERCENT) / 100
      );
    }, Promise.resolve(0));
  }

  async increaseFundFor(increaseAmount: number, fund: Fund) {
    const { amount, investor, id } = fund;

    const increasedFund = Number(amount) + increaseAmount;

    if (!fund.investor) return;

    const oldProfit = await this.profitsService.findOneById(investor.id);
    const increasedProfit = Number(oldProfit.amount) + increaseAmount;

    await this.fundsService.update(id, { amount: increasedFund });
    await this.profitsService.update(oldProfit.id, {
      amount: increasedProfit,
    });
  }
}
