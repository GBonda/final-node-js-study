import { MigrationInterface, QueryRunner } from 'typeorm';

export class DbInitialize1674294656464 implements MigrationInterface {
  name = 'DbInitialize1674294656464';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "funds" ("id" SERIAL NOT NULL, "amount" integer NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "investorId" integer, CONSTRAINT "REL_56c1ec901fe667408cdab1e1cc" UNIQUE ("investorId"), CONSTRAINT "PK_d785f4bb8f680f3febd40718f68" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "users" ("id" SERIAL NOT NULL, "username" character varying NOT NULL, "email" character varying NOT NULL, "password" character varying, "isAdmin" boolean NOT NULL DEFAULT false, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "UQ_97672ac88f789774dd47f7c8be3" UNIQUE ("email"), CONSTRAINT "PK_a3ffb1c0c8416b9fc6f907b7433" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "invitees" ("id" SERIAL NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "userId" integer, "invitorId" integer, CONSTRAINT "REL_c96df91bb65ab9c10474684170" UNIQUE ("userId"), CONSTRAINT "PK_49807fe943a4c77bff2e1dea72d" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "invitors" ("id" SERIAL NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "investorId" integer, CONSTRAINT "REL_848a35231e246f65fb56ad4a40" UNIQUE ("investorId"), CONSTRAINT "PK_f2899328b408579149e5bed3869" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "profits" ("id" SERIAL NOT NULL, "amount" integer NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "investorId" integer, CONSTRAINT "REL_3864867e4d6614d98904d0934e" UNIQUE ("investorId"), CONSTRAINT "PK_92ff66480b639c3292c6c7e7b2d" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "investors" ("id" SERIAL NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "userId" integer, CONSTRAINT "REL_5fc2494c4bcbbca3c4729921ff" UNIQUE ("userId"), CONSTRAINT "PK_7ab129212e4ce89e68d6a27ea4e" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `ALTER TABLE "funds" ADD CONSTRAINT "FK_56c1ec901fe667408cdab1e1cc6" FOREIGN KEY ("investorId") REFERENCES "investors"("id") ON DELETE SET NULL ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "invitees" ADD CONSTRAINT "FK_c96df91bb65ab9c104746841702" FOREIGN KEY ("userId") REFERENCES "users"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "invitees" ADD CONSTRAINT "FK_e9de6bcca845866ada44851af58" FOREIGN KEY ("invitorId") REFERENCES "invitors"("id") ON DELETE SET NULL ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "invitors" ADD CONSTRAINT "FK_848a35231e246f65fb56ad4a407" FOREIGN KEY ("investorId") REFERENCES "investors"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "profits" ADD CONSTRAINT "FK_3864867e4d6614d98904d0934ea" FOREIGN KEY ("investorId") REFERENCES "investors"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "investors" ADD CONSTRAINT "FK_5fc2494c4bcbbca3c4729921ff2" FOREIGN KEY ("userId") REFERENCES "users"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "investors" DROP CONSTRAINT "FK_5fc2494c4bcbbca3c4729921ff2"`,
    );
    await queryRunner.query(
      `ALTER TABLE "profits" DROP CONSTRAINT "FK_3864867e4d6614d98904d0934ea"`,
    );
    await queryRunner.query(
      `ALTER TABLE "invitors" DROP CONSTRAINT "FK_848a35231e246f65fb56ad4a407"`,
    );
    await queryRunner.query(
      `ALTER TABLE "invitees" DROP CONSTRAINT "FK_e9de6bcca845866ada44851af58"`,
    );
    await queryRunner.query(
      `ALTER TABLE "invitees" DROP CONSTRAINT "FK_c96df91bb65ab9c104746841702"`,
    );
    await queryRunner.query(
      `ALTER TABLE "funds" DROP CONSTRAINT "FK_56c1ec901fe667408cdab1e1cc6"`,
    );
    await queryRunner.query(`DROP TABLE "investors"`);
    await queryRunner.query(`DROP TABLE "profits"`);
    await queryRunner.query(`DROP TABLE "invitors"`);
    await queryRunner.query(`DROP TABLE "invitees"`);
    await queryRunner.query(`DROP TABLE "users"`);
    await queryRunner.query(`DROP TABLE "funds"`);
  }
}
