import { MigrationInterface, QueryRunner } from 'typeorm';

export class FundsAndProfitDecimalAmount1674364538306
  implements MigrationInterface
{
  name = 'FundsAndProfitDecimalAmount1674364538306';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "funds" DROP COLUMN "amount"`);
    await queryRunner.query(
      `ALTER TABLE "funds" ADD "amount" numeric NOT NULL`,
    );
    await queryRunner.query(`ALTER TABLE "profits" DROP COLUMN "amount"`);
    await queryRunner.query(
      `ALTER TABLE "profits" ADD "amount" numeric NOT NULL`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "profits" DROP COLUMN "amount"`);
    await queryRunner.query(
      `ALTER TABLE "profits" ADD "amount" integer NOT NULL`,
    );
    await queryRunner.query(`ALTER TABLE "funds" DROP COLUMN "amount"`);
    await queryRunner.query(
      `ALTER TABLE "funds" ADD "amount" integer NOT NULL`,
    );
  }
}
