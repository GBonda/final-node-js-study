import { MigrationInterface, QueryRunner } from 'typeorm';

export class UpdateUserEntity1675196328194 implements MigrationInterface {
  name = 'UpdateUserEntity1675196328194';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "users" RENAME COLUMN "isAdmin" TO "roles"`,
    );
    await queryRunner.query(`ALTER TABLE "users" DROP COLUMN "roles"`);
    await queryRunner.query(
      `ALTER TABLE "users" ADD "roles" character varying NOT NULL DEFAULT 'user'`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "users" DROP COLUMN "roles"`);
    await queryRunner.query(
      `ALTER TABLE "users" ADD "roles" boolean NOT NULL DEFAULT false`,
    );
    await queryRunner.query(
      `ALTER TABLE "users" RENAME COLUMN "roles" TO "isAdmin"`,
    );
  }
}
