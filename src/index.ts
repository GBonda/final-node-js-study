import { AppDataSource } from './data-source';

AppDataSource.initialize()
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  .then(async () => {})
  .catch((error) => console.log(error));
