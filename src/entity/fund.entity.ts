import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToOne,
  JoinColumn,
} from 'typeorm';
import { Investor } from './investor.entity';

@Entity('funds')
export class Fund {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'decimal' })
  amount: number;

  @OneToOne(() => Investor, (investor) => investor.fund, {
    onDelete: 'SET NULL',
  })
  @JoinColumn()
  investor: Investor;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;
}
