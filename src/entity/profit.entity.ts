import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToOne,
  JoinColumn,
} from 'typeorm';
import { Investor } from './investor.entity';

@Entity('profits')
export class Profit {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'decimal' })
  amount: number;

  @OneToOne(() => Investor, (investor) => investor.profit, {
    onDelete: 'CASCADE',
  })
  @JoinColumn()
  investor: Investor;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;
}
