import {
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  OneToOne,
  OneToMany,
  JoinColumn,
} from 'typeorm';
import { Investor } from './investor.entity';
import { Invitee } from './invitee.entity';

@Entity('invitors')
export class Invitor {
  @PrimaryGeneratedColumn()
  id: number;

  @OneToOne(() => Investor, (investor) => investor.invitor, {
    onDelete: 'CASCADE',
  })
  @JoinColumn()
  investor: Investor;

  @OneToMany(() => Invitee, (invitee) => invitee.invitor)
  invitees: Invitee[];

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;
}
