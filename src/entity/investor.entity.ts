import {
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  OneToOne,
  JoinColumn,
} from 'typeorm';
import { Fund } from './fund.entity';
import { Invitor } from './invitor.entity';
import { Profit } from './profit.entity';
import { User } from './user.entity';

@Entity('investors')
export class Investor {
  @PrimaryGeneratedColumn()
  id: number;

  @OneToOne(() => User, (user) => user.investor, { onDelete: 'CASCADE' })
  @JoinColumn()
  user: User;

  @OneToOne(() => Invitor, (invitor) => invitor.investor)
  invitor: Invitor;

  @OneToOne(() => Fund, (fund) => fund.investor)
  fund: Fund;

  @OneToOne(() => Profit, (profit) => profit.investor)
  profit: Profit;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;
}
