import {
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  OneToOne,
  ManyToOne,
  JoinColumn,
} from 'typeorm';
import { Invitor } from './invitor.entity';
import { User } from './user.entity';

@Entity('invitees')
export class Invitee {
  @PrimaryGeneratedColumn()
  id: number;

  @OneToOne(() => User, (user) => user.invitee, { onDelete: 'CASCADE' })
  @JoinColumn()
  user: User;

  @ManyToOne(() => Invitor, (invitor) => invitor.invitees, {
    onDelete: 'SET NULL',
  })
  invitor: Invitor;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;
}
