import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToOne,
} from 'typeorm';
import { Role } from '../common/enums/role.enum';
import { Investor } from './investor.entity';
import { Invitee } from './invitee.entity';

@Entity('users')
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  username: string;

  @Column({ unique: true })
  email: string;

  @Column({ nullable: true })
  password: string;

  @Column({ default: Role.User, type: 'enum', enum: Role })
  role: Role;

  @OneToOne(() => Investor, (investor) => investor.user)
  investor: Investor;

  @OneToOne(() => Invitee, (invitee) => invitee.user)
  invitee: Invitee;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;
}
